package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"math"
	"os"
)

const (
	maxNumberSymbols = 100 /* max number of symbols in array */
	bitsInByte = 8         /* bits in byte */
	smallNumber = 0.000001 /* small number for comparing */
)

/* gaps for Shell sort */
var (
	interval = [7] int {301, 132, 57, 23, 10, 4, 1}
)

/* function to handle error */
func handleError(err error) {

	/* handle error */
	if err != nil {
		log.Fatal(err)
	}

}

/* function to read row with printing of message */
func readRow(mess string, row *string) {

	/* declarations of variable */
	var (
		err error
	)

	/* print message and read row */
	fmt.Println(mess)
	_, err = fmt.Scanln(row)
	handleError(err)

}

/* function to calculate symbols in file */
func calculateSymbolsInFile(filename string) (map[int32]int, int) {

	/* declarations of variables */
	var (
		err      error
		reader   *bufio.Reader
		file     *os.File
		mapSymb  map[int32]int
		length   int
		line     []byte
		row      string
	)

	/* open file */
	file, err = os.Open(filename)
	handleError(err)

	/* create map */
	mapSymb = make(map[int32]int)

	/* initial length */
	length = 0
	/* create reader and read file */
	reader = bufio.NewReader(file)
	for {

		/* read one line */
		line, _, err = reader.ReadLine()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}

		/* convert to string */
		row = string(line)
		/* add length of row to total length */
		length += len(row)
		/* handle row */
		for _, c := range row {
			/* check existence and add to counter */
			if _, ok := mapSymb[c]; ok {
				mapSymb[c]++
			} else {
				mapSymb[c] = 1
			}
		}

	}

	/* close file */
	err = file.Close()
	handleError(err)

	/* return answer */
	return mapSymb, length

}

/* function to make statics */
func makeStatics(mapSymb map[int32]int, length int) ([]float64, []int32, float64) {

	/* declarations of variables */
	var (
		freq 	 	[]int
		prob 	 	[]float64
		symb 	 	[]int32
		i    	 	int
		currSymb 	int32
		currFreq 	int
		numberBits  float64
	)

	/* allocate arrays */
	freq = make([]int, 0, maxNumberSymbols)
	prob = make([]float64, 0, maxNumberSymbols)
	symb = make([]int32, 0, maxNumberSymbols)

	/* fill arrays using map of symbols */
	for currSymb, currFreq = range mapSymb {
		freq = append(freq, currFreq)
		prob = append(prob, 0.0)
		symb = append(symb, currSymb)
	}

	/* calculate probability */
	i = 0
	for i < len(freq) {
		prob[i] = float64(freq[i]) / float64(length)
		i++
	}

	/* calculate number of bits */
	i = 0
	numberBits = 0.0
	for i < len(prob) {
		if prob[i] > smallNumber {
			numberBits += prob[i] * math.Log(prob[i])
		}
		i++
	}
	numberBits *= (-1.0) * float64(length) * math.Log2(math.E)

	/* return answer */
	return prob, symb, numberBits

}

/* function to write results */
func writeResults(prob []float64, symb []int32, numberBits float64, length int) {

	/* declarations of variables */
	var (
		i, i1, i2 int
		currSymb  int32
		currProb  float64
	)

	/* make Shell sort */
	i = len(interval) - 1
	for i >= 0 {
		i1 = interval[i]
		for i1 < len(prob) {
			i2 = i1 - interval[i]
			for i2 >= 0 {
				if symb[i2] > symb[i2 + interval[i]] {
					currSymb = symb[i2]
					symb[i2] = symb[i2 + interval[i]]
					symb[i2 + interval[i]] = currSymb
					currProb = prob[i2]
					prob[i2] = prob[i2 + interval[i]]
					prob[i2 + interval[i]] = currProb
				}
				i2 -= interval[i]
			}
			i1++
		}
		i--
	}

	/* write result */

	/* single values */
	fmt.Printf("Number of necessary bits: %f\n", numberBits)
	fmt.Printf("Number of real bits: %d\n", bitsInByte * length)
	fmt.Printf("Entropy: %f\n", numberBits / float64(bitsInByte * length))

	/* table values */
	i = 0
	for i < len(prob) {
		fmt.Printf("Probability: %f, Symbol: %s\n", prob[i], string(symb[i]))
		i++
	}

}

/* entry point */
func main() {

	/* declarations of variables */
	var (
		filename   string
		length     int
		mapSymb    map[int32]int
		prob       []float64
		symb       []int32
		numberBits float64
	)

	/* read filename */
	readRow("Name of file:", &filename)
	/* calculate symbols in file */
	mapSymb, length = calculateSymbolsInFile(filename)
	/* make statics about symbols */
	prob, symb, numberBits = makeStatics(mapSymb, length)
	/* write results to user */
	writeResults(prob, symb, numberBits, length)

}